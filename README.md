# TortoiseRacing

See [https://www.codewars.com/kata/tortoise-racing]()

## Introduction

Tortoise Racing is a kata which aims to write a program that calculates the time (hours, minutes, seconds) 
needed by a car B to catch another car A, given two speeds v1 (A's speed, integer > 0) and v2 (B's speed, integer > 0) 
and a lead g (integer > 0).

The program must start with `race(v1,v2,g)`.

### TDD approach

This Kata has to be solved using Test Driven Development workflow:

1. Write a simple failing test
1. Write enough production code to make the test green
1. Refactor the production code to respect SOLID and Clean Code principle, then run the test again
1. Refactor the test code to respect SOLID and Clean Code principle, then run the test again
1. Restart with 1

### PDD approach

Property Driven Development is TDD where tests are phrased based on invariants or properties of the system under development.
This completes traditional tests based on specific samples, and allows to be closer to the mathematical proof of correctness 
using essential and algebric properties. Variables of invariants under test are provisoned with values provided by generators.

This kata can be solved in a property driven way, in fact a linear calculation algorithm has to be defined and implemented, 
based on some invariants that combine elementary physics with time decomposition.

## Resolution

Elixir is the chosen language for this task. Let's create a project first.

Run `mix new tortoise_racing_kata_elixir --app tortoise_racing`

This creates the kata project. Two test-dependencies are needed:

* **mix-test-watch** which is a tool that runs the tests after any change made on the code.
* **propcheck** which is the library for property-based testing, an equivalent to the QuickCheck library.

Run `mix test.watch`

### A failing test

A first invariant says that B cannot reach A with same speeds and positive lead.

The following test runs per default 100 times with randomly generated values for v and g on the given [1..100] discrete interval:

```
property "Same speed, A has a positive lead, impossible for B to reach A" do
  forall v <- choose(1, 100) do
    forall g <- choose(1, 100) do
      assert TortoiseRacing.race(v, v, g) == [-1, -1, -1]
    end
  end
end
```

This does not compile because the TortoiseRacing module does not exist.  Let’s write just enough production code so that it compiles.

```
defmodule TortoiseRacing do
  @moduledoc """
  Documentation for TortoiseRacing.
  """
  def race(v1, v2, g) do
  end
end
```

This time we have a failing test; we have to write the necessary production code to make it work.

```
defmodule TortoiseRacing do
  @moduledoc """
  Documentation for TortoiseRacing.
  """
  def race(v1, v2, g), do: [-1, -1, -1]
end
```

This time the test is green. The test report mentions the seed used by the random generator when provisioning values
in order to be reproduced the values constellation easily.

```
Running tests...
.........

Finished in 0.1 seconds
1 properties, 1 test, 0 failures

Randomized with seed 424286

```

### Cycle One

A next invariant says that B cannot reach A if A is quicker.

```
property "A is quicker than B, A has a positive lead, impossible for B to reach A" do
  forall v <- choose(1, 100) do
    forall g <- choose(1, 100) do
      assert TortoiseRacing.race(v + 1, v, g) == [-1, -1, -1]
    end
  end
end
```

Just enough production code to make pass this new test and the previous one.

```
def race(v1, v2, _) do
  cond do
    v1 >= v2 -> [-1, -1, -1]
  end
end
```

No refactoring of production and test codes is necessary at this point.

### Cycle Two

A next invariant says that B is able to reach A in one hour if speed difference is one foot and lead is one foot.

```
property "B with 1 foot more per hour than A, can reach A who has 1 foot lead in 1 hour" do
  forall v <- choose(1, 100) do
    assert TortoiseRacing.race(v, v + 1, 1) == [1, 0, 0]
  end
end
```

Just enough production code to make pass this new test and the previous ones.

```
def race(v1, v2, _) do
  cond do
    v1 >= v2 -> [-1, -1, -1]
    true -> [1, 0, 0]
  end
end
```

No refactoring of production and test codes is necessary at this point, since neither duplication nor SOLID violations occur.

### Cycle Three

A next invariant says that more generally B is able to reach A in g hours if the difference of speed is one foot and lead is g feet.

```
property "B with 1 foot more per hour than A, can reach A who has g feet lead in g hours" do
  forall v <- choose(1, 100) do
    forall g <- choose(1, 100) do
      assert TortoiseRacing.race(v, v + 1, g) == [g, 0, 0]
    end
  end
end
```

Just enough production code to make pass this new test and the previous ones.

```
def race(v1, v2, g) do
  cond do
    v1 >= v2 -> [-1, -1, -1]
    true -> [g, 0, 0]
  end
end
```

At this point production code still does not need to be refactored, but test code should, since this last invariant covers the previous less general one.

### Cycle Four

A next invariant says that B is able to reach A in one hour if the lead equals to the difference of speed.

```
property "B with dv feet more than A per hour, can reach A who has g=dv feet lead in 1 hour" do
  forall v <- choose(1, 100) do
    forall dv <- choose(1, 100) do
      assert TortoiseRacing.race(v, v + dv, dv) == [1, 0, 0]
    end
  end
end
```

Just enough production code to make pass this new test and the previous ones.

```
def race(v1, v2, g) do
  cond do
    v1 >= v2 -> [-1, -1, -1]
    true -> [div(g, v2 - v1), 0, 0]
  end
end
```

No refactoring of production and test codes is necessary at this point, since neither duplication nor SOLID violations occur.

### Cycle Five

A next invariant says that B is able to reach A in k hours if the difference of speed is a divisor of the lead, the factor being k.

```
property "B with dv feet more than A per hour, dv < g, can reach A who has g=k*dv feet lead in k hours" do
  forall v <- choose(1, 100) do
    forall dv <- choose(1, 100) do
      forall k <- choose(1, 10) do
        assert TortoiseRacing.race(v, v + dv, k * dv) == [k, 0, 0]
      end
    end
  end
end
```

Just enough production code to make pass this new test and the previous ones.

```
def race(v1, v2, g) do
  cond do
    v1 >= v2 -> [-1, -1, -1]
    true -> [div(g, v2 - v1), div(rem(g, v2 - v1) * 60, v2 - v1), 0]
  end
end
```

At this point production code still does not need to be refactored, but test code should, since this last invariant covers the previous less general one.

### Cycle Six

A next invariant says that B is able to reach A in k minutes if the difference of speed is the product of 60 by a given factor n, and the lead is the product of k by n.

```
property "B with dv feet more than A per hour, dv > g, dv=60*n, B can reach A who has g=k*n feet lead in k minutes" do
  forall v <- choose(1, 100) do
    forall n <- choose(1, 10) do
      forall k <- choose(1, 10) do
        assert TortoiseRacing.race(v, v + 60 * dv, k * n) == [0, k, 0]
      end
    end
  end
end
```

Just enough production code to make pass this new test and the previous ones.

```
def race(v1, v2, g) do
  cond do
    v1 >= v2 -> [-1, -1, -1]
    true -> [div(g, v2 - v1), div(rem(g, v2 - v1) * 60, v2 - v1), 0]
  end
end
```

At this point production code still does not need to be refactored, but test code should, since this last invariant covers the previous less general one.

### Cycle Seven

A next invariant says that B is able to reach A in k seconds if the difference of speed is the product of 3600 by a given factor n, and the lead is the product of k by n.

```
property "B with dv feet more than A per hour, dv > g, dv=3600*n, B can reach A who has g=k*n feet lead in k minutes" do
  forall v <- choose(1, 100) do
    forall n <- choose(1, 10) do
      forall k <- choose(1, 10) do
        assert TortoiseRacing.race(v, v + 3600 * dv, k * n) == [0, 0, k]
      end
    end
  end
end
```

Just enough production code to make pass this new test and the previous ones.

```
def race(v1, v2, g) do
  cond do
    v1 >= v2 -> [-1, -1, -1]
    true ->
      hours = {div(g, v2 - v1), rem(g, v2 - v1)}
      minutes = {div(elem(hours, 1) * 60, v2 - v1), rem(elem(hours, 1) * 60, v2 - v1)}
      seconds = div(elem(minutes, 1) * 60, v2 - v1)
      [elem(hours, 0), elem(minutes, 0), seconds]
  end
end
```

No refactoring of production and test codes is necessary at this point, since neither duplication nor SOLID violations occur.

### Cycle Eight

The set of invariants being complete, this last cycle proposes to run specific test samples. 

```
test "should satisfy these given samples" do
  assert TortoiseRacing.race(720, 850, 70) == [0, 32, 18]
  assert TortoiseRacing.race(80, 91, 37) == [3, 21, 49]
end
```

All the tests still pass, no more production code is required, code design does not contain any design smell. 



  


