defmodule TortoiseRacing do
  @moduledoc """
  See https://www.codewars.com/kata/55e2adece53b4cdcb900006c
  """

  @doc """
  More generally: given two speeds v1 (A's speed, integer > 0) and v2 (B's speed, integer > 0) and a lead g (integer > 0) how long will it take B to catch A?
  The result will be an array [hour, min, sec] which is the time needed in hours, minutes and seconds (round down to the nearest second).
  If v1 >= v2 then return None or {-1, -1, -1}.

  Tortoises don't care about fractions of seconds
  Think of calculation by hand using only integers (in your code use or simulate integer division)
  """

  def race(v1, v2, g), do: race_7(v1, v2, g)

  defp race_7(v1, v2, g) do
    cond do
      v1 >= v2 ->
        [-1, -1, -1]

      true ->
        hours = {div(g, v2 - v1), rem(g, v2 - v1)}
        minutes = {div(elem(hours, 1) * 60, v2 - v1), rem(elem(hours, 1) * 60, v2 - v1)}
        seconds = div(elem(minutes, 1) * 60, v2 - v1)
        [elem(hours, 0), elem(minutes, 0), seconds]
    end
  end

  defp race_6(v1, v2, g) do
    cond do
      v1 >= v2 -> [-1, -1, -1]
      true -> [div(g, v2 - v1), div(rem(g, v2 - v1) * 60, v2 - v1), 0]
    end
  end

  defp race_5(v1, v2, g) do
    cond do
      v1 >= v2 -> [-1, -1, -1]
      true -> [div(g, v2 - v1), 0, 0]
    end
  end

  defp race_4(v1, v2, g) do
    cond do
      v1 >= v2 -> [-1, -1, -1]
      true -> [g, 0, 0]
    end
  end

  defp race_3(v1, v2, _) do
    cond do
      v1 >= v2 -> [-1, -1, -1]
      true -> [1, 0, 0]
    end
  end

  defp race_2(v1, v2, _) do
    cond do
      v1 >= v2 -> [-1, -1, -1]
    end
  end

  defp race_1(_, _, _), do: [-1, -1, -1]
end
