defmodule TortoiseRacingTest do
  use ExUnit.Case
  use PropCheck
  doctest TortoiseRacing

  # step 1
  property "Same speed, A has a positive lead, impossible for B to reach A" do
    forall v <- choose(1, 100) do
      forall g <- choose(1, 100) do
        assert TortoiseRacing.race(v, v, g) == [-1, -1, -1]
      end
    end
  end

  # step 2
  property "A is quicker than B, A has a positive lead, impossible for B to reach A" do
    forall v <- choose(1, 100) do
      forall g <- choose(1, 100) do
        assert TortoiseRacing.race(v + 1, v, g) == [-1, -1, -1]
      end
    end
  end

  # step 3
  property "B with 1 foot more per hour than A, can reach A who has 1 foot lead in 1 hour" do
    forall v <- choose(1, 100) do
      assert TortoiseRacing.race(v, v + 1, 1) == [1, 0, 0]
    end
  end

  # step 4
  property "B with 1 foot more per hour than A, can reach A who has g feet lead in g hours" do
    forall v <- choose(1, 100) do
      forall g <- choose(1, 100) do
        assert TortoiseRacing.race(v, v + 1, g) == [g, 0, 0]
      end
    end
  end

  # step 5
  property "B with dv feet more than A per hour, can reach A who has g=dv feet lead in 1 hour" do
    forall v <- choose(1, 100) do
      forall dv <- choose(1, 100) do
        assert TortoiseRacing.race(v, v + dv, dv) == [1, 0, 0]
      end
    end
  end

  property "B with dv feet more than A per hour, dv < g, can reach A who has g=k*dv feet lead in k hours" do
    forall v <- choose(1, 100) do
      forall dv <- choose(1, 100) do
        forall k <- choose(1, 10) do
          assert TortoiseRacing.race(v, v + dv, k * dv) == [k, 0, 0]
        end
      end
    end
  end

  # step 6
  property "B with dv feet more than A per hour, dv > g, dv=60*n, g=k*n, B can reach A who has g feet lead in k minutes" do
    forall v <- choose(1, 100) do
      forall n <- choose(1, 10) do
        forall k <- choose(1, 10) do
          assert TortoiseRacing.race(v, v + 60 * n, k * n) == [0, k, 0]
        end
      end
    end
  end

  # step 7
  property "B with dv feet more than A per hour, dv > g, dv=3600*n, g=k*n, B can reach A who has g feet lead in k seconds" do
    forall v <- choose(1, 100) do
      forall n <- choose(1, 10) do
        forall k <- choose(1, 10) do
          assert TortoiseRacing.race(v, v + 3600 * n, k * n) == [0, 0, k]
        end
      end
    end
  end

  test "should satisfy these given samples" do
    assert TortoiseRacing.race(720, 850, 70) == [0, 32, 18]
    assert TortoiseRacing.race(80, 91, 37) == [3, 21, 49]
  end
end
